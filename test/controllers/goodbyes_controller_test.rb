require "test_helper"

class GoodbyesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @goodbye = goodbyes(:one)
  end

  test "should get index" do
    get goodbyes_url
    assert_response :success
  end

  test "should get new" do
    get new_goodbye_url
    assert_response :success
  end

  test "should create goodbye" do
    assert_difference("Goodbye.count") do
      post goodbyes_url, params: { goodbye: {  } }
    end

    assert_redirected_to goodbye_url(Goodbye.last)
  end

  test "should show goodbye" do
    get goodbye_url(@goodbye)
    assert_response :success
  end

  test "should get edit" do
    get edit_goodbye_url(@goodbye)
    assert_response :success
  end

  test "should update goodbye" do
    patch goodbye_url(@goodbye), params: { goodbye: {  } }
    assert_redirected_to goodbye_url(@goodbye)
  end

  test "should destroy goodbye" do
    assert_difference("Goodbye.count", -1) do
      delete goodbye_url(@goodbye)
    end

    assert_redirected_to goodbyes_url
  end
end
