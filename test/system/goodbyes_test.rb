require "application_system_test_case"

class GoodbyesTest < ApplicationSystemTestCase
  setup do
    @goodbye = goodbyes(:one)
  end

  test "visiting the index" do
    visit goodbyes_url
    assert_selector "h1", text: "Goodbyes"
  end

  test "should create goodbye" do
    visit goodbyes_url
    click_on "New goodbye"

    click_on "Create Goodbye"

    assert_text "Goodbye was successfully created"
    click_on "Back"
  end

  test "should update Goodbye" do
    visit goodbye_url(@goodbye)
    click_on "Edit this goodbye", match: :first

    click_on "Update Goodbye"

    assert_text "Goodbye was successfully updated"
    click_on "Back"
  end

  test "should destroy Goodbye" do
    visit goodbye_url(@goodbye)
    click_on "Destroy this goodbye", match: :first

    assert_text "Goodbye was successfully destroyed"
  end
end
