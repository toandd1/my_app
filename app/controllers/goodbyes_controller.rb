class GoodbyesController < ApplicationController
  before_action :set_goodbye, only: %i[ show edit update destroy ]

  # GET /goodbyes or /goodbyes.json
  def index
    @goodbyes = Goodbye.all
  end

  # GET /goodbyes/1 or /goodbyes/1.json
  def show
  end

  # GET /goodbyes/new
  def new
    @goodbye = Goodbye.new
  end

  # GET /goodbyes/1/edit
  def edit
  end

  # POST /goodbyes or /goodbyes.json
  def create
    @goodbye = Goodbye.new(goodbye_params)

    respond_to do |format|
      if @goodbye.save
        format.html { redirect_to goodbye_url(@goodbye), notice: "Goodbye was successfully created." }
        format.json { render :show, status: :created, location: @goodbye }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @goodbye.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goodbyes/1 or /goodbyes/1.json
  def update
    respond_to do |format|
      if @goodbye.update(goodbye_params)
        format.html { redirect_to goodbye_url(@goodbye), notice: "Goodbye was successfully updated." }
        format.json { render :show, status: :ok, location: @goodbye }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @goodbye.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goodbyes/1 or /goodbyes/1.json
  def destroy
    @goodbye.destroy

    respond_to do |format|
      format.html { redirect_to goodbyes_url, notice: "Goodbye was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goodbye
      @goodbye = Goodbye.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def goodbye_params
      params.fetch(:goodbye, {})
    end
end
