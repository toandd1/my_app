class User < ApplicationRecord
  has_many :microposts
  validates :name, length: { maximum: 14 }, presence: true
  validates :email, confirmation: true , presence: true
end
