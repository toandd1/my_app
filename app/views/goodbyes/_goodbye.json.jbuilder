json.extract! goodbye, :id, :created_at, :updated_at
json.url goodbye_url(goodbye, format: :json)
