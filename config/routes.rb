Rails.application.routes.draw do
  resources :users
  resources :microposts
  root "users#index"
  get '/1/edit', to: 'user#edit', as: 'user_edit'
  get '/goodbye', to: 'goodbyes#index', as: 'patient'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
end
